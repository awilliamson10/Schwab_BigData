library("Biobase")
library("GEOquery")
library("limma")
library("edgeR")
library("DESeq2")
library("genefilter")
library("ClassDiscovery")
library("gplots")
library("lattice") 
library("stringi")
library("readr")
library("dplyr")
library("AnnotationDbi")
library("org.Hs.eg.db")
library("dplyr")

#reading in the raw dataset
GSE69618_raw_reads <- read_delim("GSE69618_raw_reads.txt", "\t", escape_double = FALSE, trim_ws = TRUE)
#collecting meta data from GEO
data <- getGEO("GSE69618")
data <- data[[1]]
#collecting meta data
pheno <- pData(data)

#-----Annotating Genes-----------
geneList <- as.matrix(GSE69618_raw_reads)
columns(org.Hs.eg.db)
geneSymbols <- mapIds(org.Hs.eg.db, keys=geneList[,1], column="SYMBOL", keytype="ACCNUM", multiVals="first")
inds <- which(!is.na(geneSymbols))
found_genes <- geneSymbols[inds]
GSE.copy <- GSE69618_raw_reads
GSE.copy <- subset(GSE69618_raw_reads, GSE69618_raw_reads$RefSeqID%in%names(found_genes))
GSE.copy$RefSeqID <- found_genes
#-----End Gene Annotation--------

#-------Creating DataFrame of Day6-------
dtP <- data.frame(pheno[24,])
dtP[2,] <- pheno[25,]
dtP[3,] <- pheno[22,]
dtP[4,] <- pheno[23,]
dtP[5,] <- pheno[18,]
dtP[6,] <- pheno[19,]
dtA <- GSE.copy[,24:25]
dtA[,3] <- GSE.copy[,22]
dtA[,4] <- GSE.copy[,23]
dtA[,5] <- GSE.copy[,18]
dtA[,6] <- GSE.copy[,19]

#-------Creating Groups Based on Gene Type
levels <- levels(dtP$supplementary_file_1)
levels[length(levels)+1] <- "A"
levels[length(levels)+1] <- "B"
levels[length(levels)+1] <- "C"
dtP$supplementary_file_1 <- factor(dtP$supplementary_file_1, levels=levels)
dtP[1:2,39] <- "A"
dtP[3:4,39] <- "C"
dtP[5:6,39] <- "B"

#removing all NA rows
dtA <- na.omit(dtA)

#-----Performing DESeq to find most Highly Differentiatied Genes
dge <- DGEList(counts=dtA, group = dtP$supplementary_file_1)
dge <- calcNormFactors(dge, method = "TMM")
dge$samples
levels(dge$samples$group)
dge <- estimateDisp(dge)

#Most highly differentiated from WT and NK samples
wtNK <- exactTest(dge,pair=c("A","B"))
topTags(wtNK)
#Most highly differentiated from WT and ZN samples
wtZN <- exactTest(dge, pair=c("A","C"))
topTags(wtZN)

#Creating tables of the Top 50 genes from each differentiation comparison
top.wtNK <- topTags(wtNK, n=50)
top.wtNK <- top.wtNK$table 
top.wtZN <- topTags(wtZN, n=50)
top.wtZN <- top.wtZN$table 
rows.wtNK <- rownames(top.wtNK)
rows.wtZN <- rownames(top.wtZN)
genewtNK <- subset(GSE.copy, rownames(GSE.copy)%in%rows.wtNK)
genewtZN <- subset(GSE.copy, rownames(GSE.copy)%in%rows.wtZN)

#Taking the common Differentations from the comparisons
gene.common.day6 <- subset(genewtNK, genewtNK$RefSeqID%in%genewtZN$RefSeqID)
#Removing the duplicated gene differentiations
gene.common.day6 <- unique(gene.common.day6$RefSeqID)

